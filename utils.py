# -*- coding: utf-8 -*-
# Author: Lewis
# Date: 2024/3/14


import logging


def get_init_logger():
    # creating a logger对象
    logger = logging.getLogger('mylogger')

    # define the default level of the logger
    logger.setLevel(logging.INFO)

    # creating a formatter
    formatter = logging.Formatter('%(asctime)s | %(levelname)s -> %(message)s')

    # creating a handler to log on the filesystem
    file_handler = logging.FileHandler('error.log', encoding='utf-8')
    file_handler.setFormatter(formatter)
    file_handler.setLevel(logging.INFO)

    # adding handlers to our logger
    logger.addHandler(file_handler)

    logger.info('this is a log message...')

    return logger


logger = get_init_logger()
