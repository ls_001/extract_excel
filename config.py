# -*- coding: utf-8 -*-
# Author: Lewis
# Date: 2024/3/13

EXCEL_NAME = "料品编码唯一（全部）.xlsx"  # excel文件名
SHEET_START = 0  # sheet表索引
DATA_ROW_START = 2  # 数据开始行
COLUMN_IMAGE = 'X'  # 图像列名
COLUMN_TEXT_1 = 'A'
COLUMN_TEXT_2 = 'B'
SAVE_SUFFIX = "extract"