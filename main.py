# -*- coding: utf-8 -*-
# Author: Lewis
# Date: 2024/3/13

from model import ExtractExcel

if __name__ == "__main__":
    ex = ExtractExcel(excel_name="料品编码唯一（全部）.xlsx")
    ex.extract_excel_image(save_suffix='extract')
    # ExtractExcel.extract_from_file(file_path='goods_list')
